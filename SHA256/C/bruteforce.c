#include <openssl/sha.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

int size = 0;
int maxLength = 8;

const char alfabeto[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

unsigned char* generateSha(char* str) {
    unsigned char* buffer = (unsigned char*)malloc(sizeof(unsigned char) * BUFSIZ);
    SHA256_CTX ctx;
    size_t len;
    SHA256_Init(&ctx);

    do {
        SHA256_Update(&ctx, str, size);
    } while (len == BUFSIZ);

    SHA256_Final(buffer, &ctx);
    return buffer;
}
unsigned long long int calcolaCombinazioni() {
    unsigned long long int b = 0;
    while (alfabeto[b] != '\0') b++;
    unsigned long long int n = 0;
    int k;
    for (k = 0; k < maxLength; k++) {
        n = (n * b) + b;
    }
    return n;
}

void creaCombinazioni(unsigned long long int n,unsigned char* inputSha) {
    unsigned long long int i = 1;
    unsigned long long int reminder = 0;
    unsigned long long int b = -1;
    while (alfabeto[b+1] != '\0') b++;
    for (i = 1; i <= n; i++) {
        unsigned long long int current = i;
        char* combinazione = (char*)malloc(20*sizeof(char));
        do {
            reminder = current % b;
            if (reminder == 0) {
                char tmp[2];
                tmp[0]= alfabeto[b];
                tmp[1] = '\0';
                combinazione = strcat(combinazione, tmp);//append(combinazione, alfabeto[b]);
                current =(long long int)( (current / b) )- 1;
            }
            else {
                    char tmp[2];
                tmp[0] = alfabeto[reminder];
                tmp[1] = '\0';
                combinazione = strcat(combinazione, tmp);
                current = (long long int)((current / b));
            }
        } while (current >0);
        unsigned char* sha = generateSha(combinazione);
        int k = 0;
        bool uguali = true;
        while (uguali && k< SHA_DIGEST_LENGTH) {
            if (sha[k] != inputSha[k])
                uguali = !uguali;
            k++;
        }
        if (uguali) {
            printf("parola trovata");
            return;
        }
    }
}
int main(int argc, char** argv)
{  
    if (argc < 3) {
        fprintf(stderr, "usage: %s string - max length\n", argv[0]);
        return 1;
    }
    while (argv[1][size] != '\0') size++;
    maxLength = atoi(argv[2]);
    printf("la parola :\t%s", argv[1]);
    printf("\n la lunghezza :\t%d", size);
    unsigned char *buffer = (unsigned char*)malloc(20*sizeof(char));
    buffer = generateSha(argv[1]);
    int lent;
    size_t len;
    printf("lunghezza = %d\n", SHA_DIGEST_LENGTH);
    unsigned long long int toPass = calcolaCombinazioni();
    creaCombinazioni(toPass, buffer);
    return 0;
}


///per compilare usare il seguente comando: gcc bruteforce.c -o main.x -L/usr/lib -lssl -lcrypto -lm
///richiede libssl (es. apt-get install libssl-dev )