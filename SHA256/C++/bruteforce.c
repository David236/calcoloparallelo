#include <atomic>
#include <cstdio>
#include <cstring>
#include <future>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <openssl/sha.h>
 
struct sha256 {
    unsigned char digest[SHA256_DIGEST_LENGTH];
    void compute(const char* str, int len) {
        SHA256((const unsigned char*)str, len, digest);
    }
    bool parse(const std::string& hash) {
        if (hash.length() != 2*SHA256_DIGEST_LENGTH) {
            std::cerr << "Invalid SHA-256 hash\n";
            return false;
        }
        const char* p = hash.c_str();
        for (int i = 0; i < SHA256_DIGEST_LENGTH; ++i, p += 2) {
            unsigned int x;
            if (sscanf(p, "%2x", &x) != 1) {
                std::cerr << "Cannot parse SHA-256 hash\n";
                return false;
            }
            digest[i] = x;
        }
        return true;
    }
};
 
bool operator==(const sha256& a, const sha256& b) {
    return memcmp(a.digest, b.digest, SHA256_DIGEST_LENGTH) == 0;
}
 
bool next_password(std::string& passwd, size_t start) {
    size_t len = passwd.length();
    for (size_t i = len - 1; i >= start; --i) {
        char c = passwd[i];
        if (c < 'z') {
            ++passwd[i];
            return true;
        }
        passwd[i] = 'A';
    }
    return false;
}
 
class password_finder {
public:
    password_finder(int);
    void find_passwords(const std::vector<std::string>&);
private:
    int length;
    void find_passwords(char);
    std::vector<std::string> hashes;
    std::vector<sha256> digests;
    std::atomic<size_t> count;
};
 
password_finder::password_finder(int len) : length(len) {}
 
void password_finder::find_passwords(char ch) {
    std::string passwd(length, 'A');
    passwd[0] = ch;
    sha256 digest;
    while (count > 0) {
        digest.compute(passwd.c_str(), length);
        for (int m = 0; m < hashes.size(); ++m) {
            if (digest == digests[m]) {
                --count;
                std::ostringstream out;
                out << "password: " << passwd << ", hash: " << hashes[m] << '\n';
                std::cout << out.str();
                break;
            }
        }
        if (!next_password(passwd, 1))
            break;
    }
}
 
void password_finder::find_passwords(const std::vector<std::string>& h) {
    hashes = h;
    digests.resize(hashes.size());
    for (int i = 0; i < hashes.size(); ++i) {
        if (!digests[i].parse(hashes[i]))
            return;
    }
    count = hashes.size();
    std::vector<std::future<void>> futures;
    const int n = 52;
    for (int i = 0; i < n; ++i) {
        char c = 'A' + i;
        futures.push_back(std::async(std::launch::async,
                [&,c]() { find_passwords(c); }));
    }
}
 
int main() {
    std::vector<std::string> hashes{
        "61be55a8e2f6b4e172338bddf184d6dbee29c98853e0a0485ecee7f27b9af0b4"}; //aaaa
        //"ed968e840d10d2d313a870bc131a4e2c311d7ad09bdf32b3418147221f51a6e2"};
        //"ed02457b5c41d964dbd2f2a609d63fe1bb7528dbe55e1abf5b52c249cd735797"};
    password_finder pf(4);
    pf.find_passwords(hashes);
    return 0;
}
/// g++ bruteforce.c -o main.x -std=c++14 -latomic -lpthread -lssl -lcrypto