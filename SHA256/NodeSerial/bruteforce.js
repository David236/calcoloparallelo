const crypto = require('crypto');
const { exit } = require('process');

let letters = "@ABCCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split('')

let target = "aaaa"
let targetHash = crypto.createHash('sha256').update(target).digest('hex')
const MAX_LENGTH = 8
console.time("c")

let b = letters.length - 1;  // base to convert to
let n = 0;
for (let k = 0; k < MAX_LENGTH; k++)
    n = (n*b)+b;  
let remainder=0;
for (let i = 1; i <= n; i++) {
    let current = i;  
    var combination = "";
    do {
        remainder = current % b;
        if (remainder == 0) {
            combination += letters[b];
            current = parseInt(current/b) - 1;
        } else {
            combination += letters[remainder];
            current = parseInt(current/b);
        }
    } while (current > 0);
    let hash = crypto.createHash('sha256').update(combination).digest('hex');
    if(targetHash==hash){
        console.log("Hash found!")
        console.timeEnd("c")
        exit(0)
    }
}