#!/usr/bin/env node

'use strict';

const zmq = require("zeromq/v5-compat"); //require('zeromq');
const isv = require('indexed-string-variation');
const yargs = require('yargs');
const bigInt = require('big-integer');
const createRouter = require('./server/createRouter');
const logger = require('./logger');

const argv = yargs
  .usage('Usage: $0 <token> [options]')
  .example('$0 1e4e888ac66f8dd41e00c5a7ac36a32a9950d271')
  .demand(1)
  .number('port')
  .default('port', 9900)
  .alias('p', 'port')
  .describe('port', 'The port used to accept incoming connections')
  .number('pubPort')
  .default('pubPort', 9901)
  .alias('P', 'pubPort')
  .describe('pubPort', 'The port used to publish signals to all the workers')
  .string('alphabet')
  .default('alphabet', isv.defaultAlphabet)
  .alias('a', 'alphabet')
  .describe('alphabet', 'The alphabet used to generate the passwords')
  .number('batchSize')
  .alias('b', 'batchSize')
  .default('batchSize', 100000)
  .describe('batchSize', 'The number of attempts assigned to every client in a batch')
  .number('start')
  .alias('s', 'start')
  .describe('start', 'The index from where to start the search')
  .default('start', 0)
  .help()
  .version()
  .check(args => {
    return true;
  })
  .argv
;

const token = argv._[0];
const port = argv.port;
const pubPort = argv.pubPort;
const alphabet = argv.alphabet;
const batchSize = bigInt(String(argv.batchSize));
const start = argv.start;
const batchSocket = zmq.socket('router');
const signalSocket = zmq.socket('pub');
const router = createRouter(
  batchSocket,
  signalSocket,
  token,
  alphabet,
  batchSize,
  start,
  logger,
  process.exit
);

batchSocket.on('message', router);

batchSocket.bind(`tcp://*:${port}`);
signalSocket.bind(`tcp://*:${pubPort}`);
logger.info(`Server listening on port ${port}, signal publish on port ${pubPort}`);

//this needs zmq bindings (es. apt-get install libzmq3-dev)
//run npm install to install required dependencies
//node server.js starts the server, then run one client per cpu thread 