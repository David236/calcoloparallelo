'use strict';

const bigInt = require('big-integer');
const crypto = require('crypto');
const readline = require('readline');

const processBatch = (token, variations, batch, cb) => {
  const chunkSize = bigInt(String(1000));
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: ''
  });

  const batchStart = bigInt(batch[0]);
  const batchEnd = bigInt(batch[1]);

  const processChunk = (from, to) => {
    let pwd;
    const progress = from.minus(batchStart).multiply(bigInt(String(100))).divide(batchEnd.minus(batchStart));
    rl.write(`>  ${progress}% (${variations(from)} - ${variations(to)})`);

    for (let i = from; i.lesser(to); i = i.add(bigInt.one)) {
      pwd = crypto.createHash('sha256').update(variations(i)).digest('hex');
      if(pwd==token){
          rl.write('', {ctrl: true, name: 'u'});
          rl.close();
          return cb(pwd, i.toString());
      }
    }

    // prepare next chunk
    from = to;
    to = bigInt.min(batchEnd, from.add(chunkSize));

    rl.write('', {ctrl: true, name: 'u'});
    if (from === to) {
      // finished, password not found
      rl.close();
      return cb();
    }

    // process next chunk
    setImmediate(() => processChunk(from, to));
  };

  const firstChunkStart = batchStart;
  const firstChunkEnd = bigInt.min(batchEnd, batchStart.add(chunkSize));
  setImmediate(() => processChunk(firstChunkStart, firstChunkEnd));
};

module.exports = processBatch;
