#!/bin/bash

num_procs=$1
num_iters=$2
num_jobs="\j" 
for ((i=0; i<num_iters; i++)); do
  while (( ${num_jobs@P} >= num_procs )); do
    wait -n
  done
  node client.js "$i" arg2 &
done
# sh runclient.sh 12 10