#include <atomic>
#include <cstdio>
#include <cstring>
#include <future>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <openssl/sha.h>
 
struct sha1 {
    unsigned char digest[SHA_DIGEST_LENGTH];
    void compute(const char* str, int len) {
        SHA1((const unsigned char*)str, len, digest);
    }
    bool parse(const std::string& hash) {
        if (hash.length() != 2*SHA_DIGEST_LENGTH) {
            std::cerr << "Invalid SHA-1 hash\n";
            return false;
        }
        const char* p = hash.c_str();
        for (int i = 0; i < SHA_DIGEST_LENGTH; ++i, p += 2) {
            unsigned int x;
            if (sscanf(p, "%2x", &x) != 1) {
                std::cerr << "Cannot parse SHA-1 hash\n";
                return false;
            }
            digest[i] = x;
        }
        return true;
    }
};
 
bool operator==(const sha1& a, const sha1& b) {
    return memcmp(a.digest, b.digest, SHA_DIGEST_LENGTH) == 0;
}
 
bool next_password(std::string& passwd, size_t start) {
    size_t len = passwd.length();
    for (size_t i = len - 1; i >= start; --i) {
        char c = passwd[i];
        if (c < 'z') {
            ++passwd[i];
            return true;
        }
        passwd[i] = 'A';
    }
    return false;
}
 
class password_finder {
public:
    password_finder(int);
    void find_passwords(const std::vector<std::string>&);
private:
    int length;
    void find_passwords(char);
    std::vector<std::string> hashes;
    std::vector<sha1> digests;
    std::atomic<size_t> count;
};
 
password_finder::password_finder(int len) : length(len) {}
 
void password_finder::find_passwords(char ch) {
    std::string passwd(length, 'A');
    passwd[0] = ch;
    sha1 digest;
    while (count > 0) {
        digest.compute(passwd.c_str(), length);
        for (int m = 0; m < hashes.size(); ++m) {
            if (digest == digests[m]) {
                --count;
                std::ostringstream out;
                out << "password: " << passwd << ", hash: " << hashes[m] << '\n';
                std::cout << out.str();
                break;
            }
        }
        if (!next_password(passwd, 1))
            break;
    }
}
 
void password_finder::find_passwords(const std::vector<std::string>& h) {
    hashes = h;
    digests.resize(hashes.size());
    for (int i = 0; i < hashes.size(); ++i) {
        if (!digests[i].parse(hashes[i]))
            return;
    }
    count = hashes.size();
    std::vector<std::future<void>> futures;
    const int n = 52;
    for (int i = 0; i < n; ++i) {
        char c = 'A' + i;
        futures.push_back(std::async(std::launch::async,
                [&,c]() { find_passwords(c); }));
    }
}
 
int main() {
    std::vector<std::string> hashes{
        //"70c881d4a26984ddce795f6f71817c9cf4480e79"}; //aaaa
        //"df51e37c269aa94d38f93e537bf6e2020b21406c"};
        "f7a9e24777ec23212c54d7a350bc5bea5477fdbb"};
    password_finder pf(6);
    pf.find_passwords(hashes);
    return 0;
}
/// g++ bruteforce.c -o main.x -std=c++14 -latomic -lpthread -lssl -lcrypto